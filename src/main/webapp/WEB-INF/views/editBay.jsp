<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Editing Bay ${bay.id} - ${bay.name}  ${bay.ip} 
</h1>
<form:form commandName="bay" style="padding:8px">
    ID - ${bay.id}<br/>
    <p>
        Name<br/>
        <form:input path="name"/>
    </p>
    <p>
        IP<br/>
        <form:input path="ip"/>
    </p>
    <input type="submit" value="Save"/>
</form:form>
</body>
</html>
