<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<title>Home</title>

</head>
<body>
 
	<h1 id="errInfo">Bay Info Query System</h1>
	<form:form commandName="query" style="padding:8px">
		<table border="1">

			<tr>
				<td><form:select path="bay">
						<form:options items="${bayList}" />
					</form:select></td>
				<td><form:select path="cmd">
						<form:options items="${cmdList}" />
					</form:select></td>


				<td><input type="submit" value="Query" /></td>
			</tr>
		</table>
	</form:form>

	<hr />

	<h1>Listing Querying Results</h1>
	<script>
		$(document).ready(function() {
			$("button").click(function(){
				  $("li").each(function(){
				    alert($(this).text())
				  });
				});
			
			$("td.cmdtd").each(function(){
				//$(this).text ( $(this).attr("id") +$(this).parent().attr("id")  );
				var other = this;
				$.ajax({
					url : '/bifs/spring/query/getjson/'+$(this).parent().attr("id")+'/'+$(this).attr("id"),
					type : 'GET',
					async : true,
					// contentType : "application/json; charset=utf-8",
					dataType : "json",
					// data : JSON.stringify(JSONObject),
					error : function(jqXHR, textStatus, errorThrown) {

						// alert(jqXHR.status + ' ' + jqXHR.responseText);
						var errInfo = document.getElementById("errInfo");
						tobeIter = jqXHR;
						errInfo.innerHTML = "error";
						for (x in tobeIter) {
							errInfo.innerHTML += x + ":" + tobeIter[x] + "<br/>";
						}
						errInfo.innerHTML += "textStatus :::" + textStatus;
					},
					success : function(data) {
						$(other).text(data[0]);

					}
				})				
			});
			
			
		})
	</script>

	<table class="queryResults">
		<c:forEach items="${qrMap}" var="v_map">
			<tr id="${v_map.key}">
				<td>${v_map.key}</td>
				<c:forEach items="${v_map.value}" var="v_list">
					<td id="${v_list}" class="cmdtd">${v_list}</td>
				</c:forEach>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
