<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Editing Cmd ${cmd.id} - ${cmd.name}  ${cmd.value} 
</h1>
<form:form commandName="cmd" style="padding:8px">
    ID - ${cmd.id}<br/>
    <p>
        Name<br/>
        <form:input path="name"/>
    </p>
    <p>
        Value<br/>
        <form:input path="value"/>
    </p>
    <input type="submit" value="Save"/>
</form:form>
</body>
</html>
