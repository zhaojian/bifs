<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
</head>
<body>
	<h1>Admin Bay</h1>
	<table class="adminList">
		<c:forEach items="${bay}" var="v_bay">
			<tr>
				<td>${v_bay.id}</td>
				<td>${v_bay.name}</td>
				<td>${v_bay.ip}</td>
				<td><a href="editBay?id=${v_bay.id}"> EDIT</a></td>
				<td><a href="deleteBay?id=${v_bay.id}"> DELETE</a></td>
			</tr>
		</c:forEach>
		<tr>
			<td><a href="editBay"> Add Bay</a></td>
		</tr>

	</table>
	
	<form action="detectBay" method="POST">
		<input type="text" name="ipRange">
		<input type="Submit" value="Submit"> 
	</form>
</body>
</html>
