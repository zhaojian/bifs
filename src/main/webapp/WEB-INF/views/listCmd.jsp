<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
</head>
<body>
	<h1>Admin Cmd</h1>
	<table class="adminList">
		<c:forEach items="${cmd}" var="v_cmd">
			<tr>
				<td>${v_cmd.id}</td>
				<td>${v_cmd.name}</td>
				<td>${v_cmd.value}</td>
				<td><a href="editCmd?id=${v_cmd.id}"> EDIT</a></td>
				<td><a href="deleteCmd?id=${v_cmd.id}"> DELETE</a></td>
			</tr>
		</c:forEach>
		<tr>
			<td><a href="editCmd"> Add Cmd</a></td>
		</tr>
	</table>
</body>
</html>
