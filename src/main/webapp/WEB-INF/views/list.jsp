<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
</head>
<body>
	<h1>Admin People</h1>
	<table class="adminList">
		<c:forEach items="${people}" var="v_person">
			<tr>
				<td>${v_person.id}</td>
				<td>${v_person.firstName}</td>
				<td>${v_person.lastName}</td>
				<td><a href="edit?id=${v_person.id}">EDIT</a></td>
				<td><a href="delete?id=${v_person.id}">DELETE</a></td>
			</tr>
		</c:forEach>
		<tr>
			<td><a href="edit"> Add Person</a></td>
		</tr>
	</table>
</body>
</html>
