<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
<link rel="stylesheet" type="text/css" href=" <c:url value="/resources/css/main.css"/>"/>
<script type="text/javascript"  src="<c:url value='/resources/scripts/jquery-1.7.1.min.js'/>"></script>
</head>
<body>
        <div id="header">
            <div id="headerTitle"> <tiles:insertAttribute name="header" /> </div>
        </div>
        <div id="content"><tiles:insertAttribute name="body" /></div>
        <div id="footer"><tiles:insertAttribute name="footer" />
        </div>
</body>
</html>
