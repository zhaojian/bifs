package com.zhaojian.controller;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import com.zhaojian.dao.CmdDao;
import com.zhaojian.model.Cmd;
import com.zhaojian.model.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
@Controller
@RequestMapping("/cmd/")
public class CmdController {
	
	private static final Logger logger = LoggerFactory.getLogger(CmdController.class);

	@Autowired
	private CmdDao cmdDao;
	
	@RequestMapping(method=RequestMethod.GET,value="editCmd")
	public ModelAndView editCmd(@RequestParam(value="id",required=false) Long id) {		
		logger.debug("Received request to edit cmd id : "+id);				
		ModelAndView mav = new ModelAndView();		
 		mav.setViewName("editCmd");
 		Cmd cmd = null;
 		if (id == null) {
 			cmd = new Cmd();
 		} else {
 			cmd = cmdDao.findOne(id);
 		}
 		
 		mav.addObject("cmd", cmd);
		return mav;
		
	}

	@RequestMapping(method=RequestMethod.GET,value="deleteCmd") 
	public String deleteCmd(@RequestParam(value="id") Long id) {
		logger.debug("Received postback on cmd "+id);	
        Cmd cmd = cmdDao.findOne(id);
        
		try{
		cmdDao.delete(cmd);
		}catch(Exception e){
			logger.error("delete failed");
		}
		return "redirect:listCmd";
		
	}
	
	@RequestMapping(method=RequestMethod.POST,value="editCmd") 
	public String saveCmd(@ModelAttribute Cmd cmd) {
		logger.debug("Received postback on cmd "+cmd);	
		try{
		cmdDao.save(cmd);
		}catch(Exception e){
			logger.error("save failed");
		}
		return "redirect:listCmd";
		
	}
	
	@RequestMapping(method=RequestMethod.GET,value="listCmd")
	public ModelAndView listCmd() {
		logger.debug("Received request to list cmds");
		ModelAndView mav = new ModelAndView();
		List<Cmd> cmd = cmdDao.findAll();
		logger.debug("Cmd Listing count = "+cmd.size());
		mav.addObject("cmd",cmd);
		mav.setViewName("listCmd");
		return mav;
		
	}
	@RequestMapping(method=RequestMethod.GET,value="listQuery")
	public ModelAndView CmdInfo() {
		logger.debug("Received request to cmds info");
		ModelAndView mav = new ModelAndView();

 		Query query = null;		
		query = new Query();
 
		mav.addObject("query",query);
		Map<String,String> cmdOption = new HashMap<String,String>();
		cmdOption.put("1","testrecord");
		cmdOption.put("2","date");
		cmdOption.put("3","Install Date");
		mav.addObject("cmdOption",cmdOption);
		mav.setViewName("cmdInfo");
		return mav;
		
	}
}
