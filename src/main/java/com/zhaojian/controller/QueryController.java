package com.zhaojian.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.lang.Thread;

import com.zhaojian.dao.BayDao;
import com.zhaojian.dao.QueryDao;
import com.zhaojian.dao.CmdDao;
import com.zhaojian.dao.QueryRecordDao;
import com.zhaojian.model.Bay;
import com.zhaojian.model.BayEditor;
import com.zhaojian.model.CmdEditor;
import com.zhaojian.model.Query;
import com.zhaojian.model.Cmd;
import com.zhaojian.TelnetRE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;

import javax.annotation.Resource;

@Controller
@Scope("prototype")
@RequestMapping("/query")
public class QueryController {

	private static final Logger logger = LoggerFactory
			.getLogger(QueryController.class);

	@Autowired
	private QueryDao queryDao;
	@Autowired
	private CmdDao cmdDao;
	@Resource
	private BayDao bayDao;
	@Autowired
	private QueryRecordDao qrDao;
	@Autowired
	private CmdEditor cEditor;
	@Autowired
	private BayEditor bEditor;
	@Autowired
	private  HttpServletRequest request;
	@InitBinder
	public void InitBinder(WebDataBinder wb) {
		wb.registerCustomEditor(Cmd.class, cEditor);
		wb.registerCustomEditor(Bay.class, bEditor);
	}

	@RequestMapping(method = RequestMethod.GET, value = "listQuery")
	public ModelAndView GetQuery() {
		ModelAndView mav = new ModelAndView();

		List<Cmd> cmdList = cmdDao.findAll();
		List<Bay> bayList = bayDao.findAll();
		Collections.sort(cmdList);
		Collections.sort(bayList);
		Query query = new Query(cmdList, bayList);

		mav.addObject("query", query);
		mav.addObject("cmdList", cmdList);
		mav.addObject("bayList", bayList);

		mav.setViewName("listQuery");
		return mav;

	}

	@RequestMapping(method = RequestMethod.POST, value = "listQuery")
	public ModelAndView PostQuery(@ModelAttribute Query query) {
		logger.info(query.toString());
		query.setUserIp(request.getRemoteAddr());
		this.queryDao.save(query);

		Map<Bay, Set<String>> map = new TreeMap<Bay, Set<String>>();

		for (Bay bay : query.getBay()) {
			Set<String> list = new TreeSet<String>();
			for (Cmd cmd : query.getCmd()) {
				list.add(cmd.toString());
			}
			map.put(bay, list);
		}
		ModelAndView mav = this.GetQuery();
		mav.addObject("qrMap", map);
		mav.addObject(query);

		return mav;
	}

	@RequestMapping(value = "/getjson/{ip}/{cmdName}", method = RequestMethod.GET)
	public @ResponseBody
	List<String> queryOnBay(@PathVariable String ip, @PathVariable String cmdName) {
		logger.info("getjson " + ip + " " + cmdName+" "+ new Date());

		Cmd cmd = cmdDao.findByName(cmdName);
		List<String> cmdList = new ArrayList<String>();
		cmdList.add(cmd.getValue());
				
		List<String> list = TelnetRE.queryRemote(ip, "sdc","adw2.0", "]", cmdList);
 
		return list;
	}

}
