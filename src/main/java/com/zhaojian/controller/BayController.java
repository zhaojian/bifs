package com.zhaojian.controller;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import com.zhaojian.dao.BayDao;
import com.zhaojian.model.Bay;
import com.zhaojian.model.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import javax.annotation.Resource;
import com.zhaojian.TelnetRE;

@Controller
@RequestMapping("/bay")
public class BayController {
	
	private static final Logger logger = LoggerFactory.getLogger(BayController.class);

	@Resource
	private BayDao bayDao;
	
	@RequestMapping(method=RequestMethod.GET,value="editBay")
	public ModelAndView editBay(@RequestParam(value="id",required=false) Long id) {		
		logger.debug("Received request to edit bay id : "+id);				
		ModelAndView mav = new ModelAndView();		
 		mav.setViewName("editBay");
 		Bay bay = null;
 		if (id == null) {
 			bay = new Bay();
 		} else {
 			bay = bayDao.findOne(id);
 		}
 		
 		mav.addObject("bay", bay);
		return mav;
		
	}
	@RequestMapping(method=RequestMethod.GET,value="deleteBay")
	public String deleteBay(@RequestParam(value="id") Long id) {		
		logger.debug("Received request to delete bay id : "+id);				
 		Bay	bay = bayDao.findOne(id);
        try{
            bayDao.delete(bay);
        }catch(Exception e){
            logger.error("delete failed");
        }
        return "redirect:listBay";
		
	}


	@RequestMapping(method=RequestMethod.POST,value="editBay") 
	public String saveBay(@ModelAttribute Bay bay) {
		logger.debug("Received postback on bay "+bay);	
		try{
			bayDao.save(bay);
		}catch(Exception e){
			logger.error("save failed");
		}
		return "redirect:listBay";
		
	}
	
	@RequestMapping(method=RequestMethod.GET,value="listBay")
	public ModelAndView listBay() {
		logger.debug("Received request to list bays");
		ModelAndView mav = new ModelAndView();
		List<Bay> bay = bayDao.findAll();
		Collections.sort(bay);
		logger.debug("Bay Listing count = "+bay.size());
		mav.addObject("bay",bay);
		mav.setViewName("listBay");
		return mav;
		
	}
	
	@RequestMapping(method=RequestMethod.POST,value="detectBay")
	public String detectBay(@RequestParam(value="ipRange") String ipRange) {
		logger.info("detecting bay range "+ipRange);
		List<String>list = new ArrayList<String>();
		list.add("hostname|||.*");
		List<String>ipList = new ArrayList<String>();
		for(int i=1; i<255;i++)
			ipList.add(ipRange+i);
		Map<String,List<String>> map = TelnetRE.queryRemotes(ipList,"sdc","adw2.0","]", list);
		System.out.println(map);
		List<Bay> bayList = bayDao.findAll();
		for(String str: map.keySet()){
			String bayName= map.get(str).get(0).trim();
			if (bayName == TelnetRE.errOffLine|| bayName.contains("Fatal"))
				continue;
			
			Bay bay = new Bay(bayName,str);
			if (bayList.contains(bay)){
				bay = bayDao.findByIp(str);
				bay.setName(bayName);			
			}
			try{
				bayDao.save(bay);
			}catch(Exception e){
				logger.error("save failed for"+ bay.getIp());
			}
		}
		
		return "redirect:listBay";
		
	}	
}
