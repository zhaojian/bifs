package com.zhaojian.model;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zhaojian.dao.BayDao;

@Component
public class BayEditor extends PropertyEditorSupport{
	private static final Logger logger = LoggerFactory.getLogger(BayEditor.class);
	
	@Autowired
	private BayDao bayDao;



	@Override
	public void setAsText(String str){
		logger.info(str);
	
		Bay bay = bayDao.findByIp(str);	
		logger.info(bay.toString());		
		setValue(bay);		



	}
	
}
