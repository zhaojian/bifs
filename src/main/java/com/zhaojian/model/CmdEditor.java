package com.zhaojian.model;

import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zhaojian.dao.CmdDao;

@Component
public class CmdEditor extends PropertyEditorSupport{
	private static final Logger logger = LoggerFactory.getLogger(CmdEditor.class);
	
	@Autowired
	private CmdDao cmdDao;



	@Override
	public void setAsText(String str){
		logger.info(str);
		Cmd cmd = cmdDao.findByName(str);		
		logger.info(cmd.toString());		
		setValue(cmd);

	}
	
}
