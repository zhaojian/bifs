package com.zhaojian.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.UniqueConstraint;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.OneToMany;
import javax.persistence.ManyToMany;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;

import java.util.Date;
import java.util.List;


@Entity
public class Query implements Serializable {

	private static final long serialVersionUID = -2308795024635690L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(columnDefinition="DATETIME")
    private Date date;
	
	@Column( nullable=false)
	private String userIp;	

    @ManyToMany( cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	@JoinColumn(insertable=false, updatable=false)
	private List<Cmd> cmd;

    @ManyToMany( cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	@JoinColumn(insertable=false, updatable=false)
	private List<Bay> bay;

	public Query() {
		this.date = new Date();
	}
	
	public Query(List<Cmd> cmd, List<Bay>bay){
		this.cmd=cmd;
		this.bay=bay;
		this.date = new Date();
	}
	
	public String getUserIp(){
		return userIp;
	}
	
	public void setUserIp(String userIp){
		this.userIp = userIp;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    public void setDate(Date date){
        this.date = date;
    }
    public Date getDate(){
        return this.date;
    }

	public List<Cmd> getCmd() {
		return cmd;
	}

	public void setCmd(List<Cmd> cmd) {
		this.cmd = cmd;
	}

	public List<Bay> getBay() {
		return bay;
	}

	public void setBay(List<Bay> bay) {
		this.bay = bay;
	}

	@Override
	public String toString() {

		return super.toString() + " cmd = " + cmd + " " + bay
				+ " id = " + id;
	}



}
