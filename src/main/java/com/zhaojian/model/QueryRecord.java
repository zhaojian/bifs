package com.zhaojian.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class QueryRecord implements Comparable<QueryRecord> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn
	private Bay bay;

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn
	private Cmd cmd;

	@Column
	private String value;

	public QueryRecord() {

	}

	public QueryRecord(Bay bay, Cmd cmd) {
		this.bay = bay;
		this.cmd = cmd;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bay getBay() {
		return bay;
	}

	public void setBay(Bay bay) {
		this.bay = bay;
	}

	public Cmd getCmd() {
		return cmd;
	}

	public void setCmd(Cmd cmd) {
		this.cmd = cmd;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueryRecord other = (QueryRecord) obj;
		if (this.bay.equals(other.getBay()) && this.cmd.equals(other.getCmd()))
			return true;
		else
			return false;
	}

	public int compareTo(QueryRecord o) {
		if (this.bay.equals(o.getBay())) {
			return this.cmd.compareTo(o.getCmd());
		} else {
			return this.bay.compareTo(o.getBay());
		}
	}
}
