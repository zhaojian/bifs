package com.zhaojian.dao;

import com.zhaojian.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonDao extends JpaRepository<Person, Long>{

	
}