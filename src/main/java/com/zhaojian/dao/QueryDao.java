package com.zhaojian.dao;

import com.zhaojian.model.Query;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QueryDao extends JpaRepository<Query, Long>{

	
}
