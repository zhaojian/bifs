package com.zhaojian.dao;

import com.zhaojian.model.Cmd;
import org.springframework.data.jpa.repository.JpaRepository; 

public interface CmdDao extends JpaRepository<Cmd, Long>{

	public Cmd findByName(String name);
}
