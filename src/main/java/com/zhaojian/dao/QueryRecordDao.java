package com.zhaojian.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zhaojian.model.QueryRecord;

public interface QueryRecordDao  extends JpaRepository<QueryRecord, Long>{

}
