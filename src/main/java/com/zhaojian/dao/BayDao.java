package com.zhaojian.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.zhaojian.model.Bay;

public interface BayDao extends JpaRepository<Bay, Long>{

    public Bay findByName(String str);
    public Bay findByIp(String ip);
	
}
