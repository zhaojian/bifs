package com.zhaojian.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import com.zhaojian.model.Bay;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

@ContextConfiguration("/test-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class BayControllerTest {
	
	@Autowired
	private DataInitializer dataInitializer;
	
	@Autowired
	private BayController bayController;
		
	@Before
	public void before() {
		dataInitializer.initData();
	}
	
	@Test
	public void shouldReturnBayListView() {
		ModelAndView mav = bayController.listBay();
		assertEquals("listBay",mav.getViewName());
		
		@SuppressWarnings("unchecked")
		List<Bay> bayList = (List<Bay>) mav.getModelMap().get("bay");
		assertNotNull(bayList);		
		assertEquals(DataInitializer.PERSON_COUNT,bayList.size());		
	}
	
	
    @Test
	public void shouldReturnNewBayWithEditMav() {
		ModelAndView mav = bayController.editBay(null);
		assertNotNull(mav);
		assertEquals("editBay", mav.getViewName());
		Object object = mav.getModel().get("bay");
		assertTrue(Bay.class.isAssignableFrom(object.getClass()));
		Bay bay = (Bay) object;
		assertNull(bay.getId());
		assertNull(bay.getName());
		assertNull(bay.getIp());		
	}
	
	@Test
	public void shouldReturnSecondBayWithEditMav() {
		Long template = dataInitializer.bayList.get(1);
		ModelAndView mav = bayController.editBay(template);
		assertNotNull(mav);
		assertEquals("editBay", mav.getViewName());
		Object object = mav.getModel().get("bay");
		assertTrue(Bay.class.isAssignableFrom(object.getClass()));
		Bay bay = (Bay) object;
		assertEquals(template,bay.getId());
	}
	
}
