package com.zhaojian.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import com.zhaojian.model.Cmd;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

@ContextConfiguration("/test-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class CmdControllerTest {
	
	@Autowired
	private DataInitializer dataInitializer;
	
	@Autowired
	private CmdController cmdController;
		
	@Before
	public void before() {
		dataInitializer.initData();
	}
	
	@Test
	public void shouldReturnCmdListView() {
		ModelAndView mav = cmdController.listCmd();
		assertEquals("listCmd",mav.getViewName());
		
		@SuppressWarnings("unchecked")
		List<Cmd> cmdList = (List<Cmd>) mav.getModelMap().get("cmd");
		assertNotNull(cmdList);		
		assertEquals(DataInitializer.PERSON_COUNT,cmdList.size());		
	}
	
	
    @Test
	public void shouldReturnNewCmdWithEditMav() {
		ModelAndView mav = cmdController.editCmd(null);
		assertNotNull(mav);
		assertEquals("editCmd", mav.getViewName());
		Object object = mav.getModel().get("cmd");
		assertTrue(Cmd.class.isAssignableFrom(object.getClass()));
		Cmd cmd = (Cmd) object;
		assertNull(cmd.getId());
		assertNull(cmd.getName());
		assertNull(cmd.getValue());		
	}
	
	@Test
	public void shouldReturnSecondCmdWithEditMav() {
		Long template = dataInitializer.cmdList.get(1);
		ModelAndView mav = cmdController.editCmd(template);
		assertNotNull(mav);
		assertEquals("editCmd", mav.getViewName());
		Object object = mav.getModel().get("cmd");
		assertTrue(Cmd.class.isAssignableFrom(object.getClass()));
		Cmd cmd = (Cmd) object;
		assertEquals(template,cmd.getId());
	}
	
}
