package com.zhaojian.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.zhaojian.model.Person;
import com.zhaojian.model.Bay;
import com.zhaojian.model.Cmd;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class DataInitializer {

	public static final int PERSON_COUNT = 3;

	@PersistenceContext
	private EntityManager entityManager;

	public List<Long> personList = new ArrayList<Long>();
    public List<Long> bayList = new ArrayList<Long>();
    public List<Long> cmdList = new ArrayList<Long>();

    public static boolean firstTime=true;
	public void initData() {

		
		personList.clear();// clear out the previous list of personList
		addPerson("Jim", "Smith");
		addPerson("Tina", "Marsh");
		addPerson("Steve", "Blair");
		entityManager.flush();
		entityManager.clear();

        bayList.clear();
        addBay("bay1","3.35.117.204");
        addBay("bay2","3.35.117.207");
        addBay("bay3","3.35.117.201");
        entityManager.flush();
        entityManager.clear();

        cmdList.clear();
        addCmd("version", "testrecord");
        addCmd("date", "date");
        addCmd("pwd","pwd");
        entityManager.flush();
        entityManager.clear();
	}

	public void addPerson(String firstName, String lastName) {
		Person p = new Person();
		p.setFirstName(firstName);
		p.setLastName(lastName);
		entityManager.persist(p);
		personList.add(p.getId());
	}
    public void addBay(String name, String ip){
        Bay bay = new Bay();
        bay.setName(name);
        bay.setIp(ip);
        entityManager.persist(bay);
        bayList.add(bay.getId());
    }

    public void addCmd(String name, String value){
        Cmd cmd = new Cmd();
        cmd.setName(name);
        cmd.setValue(value);
        entityManager.persist(cmd);
        cmdList.add(cmd.getId());
    }
	
	public EntityManager getEntityManager() {
		return entityManager;
	}
}
