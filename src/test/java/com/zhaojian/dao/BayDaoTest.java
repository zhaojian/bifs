package com.zhaojian.dao;

import java.util.List;

import com.zhaojian.controller.DataInitializer;
import com.zhaojian.model.Bay;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration("/test-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class BayDaoTest {

	@Autowired
	private BayDao bayDao;

	@Autowired
	private DataInitializer dataInitializer;

	@Before
	public void prepareData() {
		dataInitializer.initData();
	}

	@Test
	public void shouldSaveABay() {
		Bay p = new Bay();
		p.setIp("3.3.3.3");
		p.setName("testBay");
		bayDao.save(p);
		Long id = p.getId();
		Assert.assertNotNull(id);
	}

	@Test
	public void shouldLoadABay() {
		Long template = dataInitializer.bayList.get(0);
		Bay p = bayDao.findOne(template);

		Assert.assertNotNull("Bay not found!", p);
		Assert.assertEquals(template, p.getId());
	}

	@Test
	public void shouldListBay() {
		List<Bay> bayList = bayDao.findAll();
 

		Bay bay = new Bay("bay1","3.35.117.204");
		Assert.assertTrue(bayList.contains(bay));
		bay = bayDao.findByIp("3.35.117.201");
		Assert.assertNotNull(bay);		
		
		bay = bayDao.findByName("bay1");
		Assert.assertNotNull(bay);
	
		
	}

}
